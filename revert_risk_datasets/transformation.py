from typing import Callable, Iterable, TypeAlias

from pyspark.sql import DataFrame, Window
from pyspark.sql import functions as F

Transformation: TypeAlias = Callable[[DataFrame], DataFrame]


def stratified_sample(
    cols: Iterable[str],
    strata_size: int,
    seed: int,
) -> Transformation:
    """Returns a same-size stratified sample based on where the strata are
    all the unique values in `cols`. The maximum size of each stratum depends
    equals `strata_size`.
    """

    def _(df: DataFrame) -> DataFrame:
        window = Window.partitionBy(*cols).orderBy(F.rand(seed))
        df = (
            df.withColumn("row_num", F.row_number().over(window))
            .filter(f"row_num <= {strata_size}")
            .drop("row_num")
        )
        return df

    return _


def with_parent_revision_history(
    parent_df: DataFrame,
    rev_id: str = "rev_id",
    parent_rev_id: str = "parent_rev_id",
    wiki_db: str = "wiki_db",
) -> Transformation:
    """Aligns the revisions in the dataframe with their parent revisions
    from `parent_df` using `rev_id` and `parent_rev_id`. The resulting dataframe
    will have all existing columns both for the child and parent revisions, with
    the parent columns prefixed with 'parent_'.
    """

    def _(df: DataFrame) -> DataFrame:
        rev_df = df.alias("rev")
        rev_parent_df = parent_df.alias("parent")

        # join conditions
        rev_id_matches = F.col(f"rev.{parent_rev_id}") == F.col(f"parent.{rev_id}")
        wiki_db_matches = F.col(f"rev.{wiki_db}") == F.col(f"parent.{wiki_db}")

        rev_df = rev_df.join(
            rev_parent_df,
            on=((rev_id_matches) & (wiki_db_matches)),
            how="inner",
        )
        rev_parent_columns = (
            F.col(f"parent.{col}").alias(f"parent_{col}")
            for col in rev_parent_df.columns
            if col not in (rev_id, wiki_db)
        )
        return rev_df.select("rev.*", *rev_parent_columns)

    return _


def filter_disputed_reverts(
    reverting_rev_df: DataFrame,
    rev_id: str = "rev_id",
    reverting_rev_id: str = "reverting_rev_id",
    is_reverted: str = "is_reverted",
    page_id: str = "page_id",
    wiki_db: str = "wiki_db",
) -> Transformation:
    """Filters all disputed reverts. A disputed revert is any revert
    whose reverting revision has also been reverted. This phenomenon
    is also known as edit wars.
    """

    def _(df: DataFrame) -> DataFrame:
        rev_df = df.alias("rev")
        reverting_df = reverting_rev_df.alias("reverter")

        # join conditions
        rev_id_matches = F.col(f"rev.{reverting_rev_id}") == F.col(f"reverter.{rev_id}")
        page_id_matches = F.col(f"rev.{page_id}") == F.col(f"reverter.{page_id}")
        wiki_db_matches = F.col(f"rev.{wiki_db}") == F.col(f"reverter.{wiki_db}")

        rev_df = rev_df.join(
            reverting_df,
            on=((rev_id_matches) & (page_id_matches) & (wiki_db_matches)),
            how="left",
        )
        # filter out any reverts where the reverting revision
        # is also reverted i.e. edit wars
        rev_df = rev_df.filter(
            F.col(f"reverter.{rev_id}").isNull() | ~F.col(f"reverter.{is_reverted}")
        )
        return rev_df.select("rev.*")

    return _


def with_wikitext(
    wikitext_df: DataFrame,
    rev_id: str = "rev_id",
    wikitext_rev_id: str = "rev_id",
    rev_text: str = "rev_text",
    wiki_db: str = "wiki_db",
) -> Transformation:
    """Returns a new dataframe by adding a column that contains the revision
    wikitext, joining with `wikitext_df` on `rev_id` and `wikitext_rev_id` columns.
    """

    def _(df: DataFrame) -> DataFrame:
        df = df.alias("rev")
        wt_df = wikitext_df.alias("wt")

        # join conditions
        rev_id_matches = F.col(f"rev.{rev_id}") == F.col(f"wt.{wikitext_rev_id}")
        wiki_db_matches = F.col(f"rev.{wiki_db}") == F.col(f"wt.{wiki_db}")

        df = df.join(
            wt_df,
            on=((rev_id_matches) & (wiki_db_matches)),
            how="inner",
        )
        return df.select("rev.*", f"wt.{rev_text}")

    return _
