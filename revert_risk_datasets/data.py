from dataclasses import dataclass
from datetime import datetime
from typing import Iterable

from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F


@dataclass(frozen=True)
class Period:
    start: datetime
    end: datetime


def standardize(column_name: str) -> str:
    match column_name.split("_"):
        case ["revision", *_]:
            return column_name.replace("revision", "rev", 1)
        case ["event", "user", *_]:
            return column_name.replace("event_user", "user", 1)
        case ["event", *_]:
            return column_name.replace("event", "rev", 1)
        case _:
            return column_name


def get_revision_history(
    spark: SparkSession,
    snapshot: str,
    wiki_dbs: Iterable[str],
) -> DataFrame:
    df = spark.table("wmf.mediawiki_history").filter(
        (F.col("snapshot") == snapshot)
        & (F.col("wiki_db").isin(*wiki_dbs))
        & (F.col("page_namespace_is_content"))
        & (F.col("revision_parent_id") != 0)
        & (F.size("event_user_is_bot_by") <= 0)
    )
    df = df.select(
        "wiki_db",
        "revision_id",
        "revision_tags",
        "page_id",
        "page_title",
        "page_first_edit_timestamp",
        "event_timestamp",
        "event_comment",
        "event_user_id",
        "event_user_is_bot_by",
        "event_user_is_anonymous",
        "event_user_groups",
        "event_user_registration_timestamp",
        F.col("event_user_text").alias("user_name"),
        F.col("event_user_revision_count").alias("user_editcount"),
        F.col("revision_parent_id").alias("parent_rev_id"),
        F.col("revision_text_bytes").alias("rev_bytes"),
        F.col("revision_is_identity_reverted").alias("is_reverted"),
        F.col("revision_first_identity_reverting_revision_id").alias(
            "reverting_rev_id"
        ),
    )
    aliased_columns = (F.col(name).alias(standardize(name)) for name in df.columns)
    return df.select(*aliased_columns)


def get_wikitext_history(
    spark: SparkSession,
    snapshot: str,
    wiki_dbs: Iterable[str],
) -> DataFrame:
    df = spark.table("wmf.mediawiki_wikitext_history").filter(
        (F.col("snapshot") == snapshot)
        & (F.col("wiki_db").isin(*wiki_dbs))
        & (F.col("page_namespace") == 0)
    )
    df = df.select(
        "wiki_db",
        "revision_id",
        "revision_text",
        "revision_timestamp",
    )
    aliased_columns = (F.col(name).alias(standardize(name)) for name in df.columns)
    return df.select(*aliased_columns)
