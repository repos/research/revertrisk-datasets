import json
import logging
from datetime import datetime
from pathlib import Path
from typing import Iterable, Optional

import knowledge_integrity.models.revertrisk_multilingual.features as rrml
import typer
from knowledge_integrity.featureset import UnsupportedWikiException
from knowledge_integrity.revision import (
    InvalidRevisionException,
    build_current_revision,
)
from knowledge_integrity.schema import RevisionSchema
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T

from revert_risk_datasets.data import Period, get_revision_history, get_wikitext_history
from revert_risk_datasets.transformation import (
    filter_disputed_reverts,
    stratified_sample,
    with_parent_revision_history,
    with_wikitext,
)

app = typer.Typer()

# TODO: We should not have to duplicate these feature names
# everywhere we want to use these models. Current alternative
# is to read the 'features' attribute on the model object but that
# would require downloading the model file which seems less than ideal.
features = [
    "change_Argument",
    "change_Category",
    "change_Comment",
    "change_ExternalLink",
    "change_Gallery",
    "change_HTMLEntity",
    "change_Heading",
    "change_List",
    "change_Media",
    "change_Paragraph",
    "change_Punctuation",
    "change_Reference",
    "change_Section",
    "change_Sentence",
    "change_Table",
    "change_Table Element",
    "change_Template",
    "change_Text",
    "change_Text Formatting",
    "change_Whitespace",
    "change_Wikilink",
    "change_Word",
    "comment",
    "insert_Argument",
    "insert_Category",
    "insert_Comment",
    "insert_ExternalLink",
    "insert_Gallery",
    "insert_HTMLEntity",
    "insert_Heading",
    "insert_List",
    "insert_Media",
    "insert_Paragraph",
    "insert_Punctuation",
    "insert_Reference",
    "insert_Section",
    "insert_Sentence",
    "insert_Table",
    "insert_Table Element",
    "insert_Template",
    "insert_Text",
    "insert_Text Formatting",
    "insert_Whitespace",
    "insert_Wikilink",
    "insert_Word",
    "is_android_app_edit",
    "is_autopatrolled",
    "is_autoreview",
    "is_autoreviewer",
    "is_bureaucrat",
    "is_editor",
    "is_extendedconfirmed",
    "is_interface-admin",
    "is_ios_app_edit",
    "is_ipblock-exempt",
    "is_mobile_app_edit",
    "is_mobile_edit",
    "is_mobile_web_edit",
    "is_patroller",
    "is_reviewer",
    "is_rollbacker",
    "is_suppressredirect",
    "is_sysop",
    "is_templateeditor",
    "is_trusted",
    "is_uploader",
    "is_visualeditor",
    "is_wikieditor",
    "page_title",
    "remove_Argument",
    "remove_Category",
    "remove_Comment",
    "remove_ExternalLink",
    "remove_Gallery",
    "remove_HTMLEntity",
    "remove_Heading",
    "remove_List",
    "remove_Media",
    "remove_Paragraph",
    "remove_Punctuation",
    "remove_Reference",
    "remove_Section",
    "remove_Sentence",
    "remove_Table",
    "remove_Table Element",
    "remove_Template",
    "remove_Text",
    "remove_Text Formatting",
    "remove_Whitespace",
    "remove_Wikilink",
    "remove_Word",
    "revision_text_bytes_diff",
    "texts_change",
    "texts_insert",
    "texts_removed",
    "user_is_anonymous",
    "wiki_db",
]


@F.udf(returnType=T.StringType())
def extract_features(rev_json: str) -> Optional[str]:
    try:
        rev = build_current_revision(rev_json)
        extracted_features = rrml.extract_features(rev, features)
        # TODO: this currently returns the features as a json string.
        # In order to return a struct we'll need to define a schema but
        # there are currently *98* all of which will need to be in the schema!
        features_json = json.dumps(extracted_features)
        return features_json
    # Revision data is invalid or language not supported by model
    except (InvalidRevisionException, UnsupportedWikiException) as err:
        logging.exception(err)
        return None


def generate_dataset(
    spark: SparkSession,
    snapshot: str,
    wiki_dbs: Iterable[str],
    period: Period,
    drop_disputed_reverts: bool,
    drop_anonymous_edits: bool,
    max_rows_per_wiki: int,
    max_files_per_wiki: int,
) -> DataFrame:
    """Generates training or testing dataset for the revert risk multilingual
    model for `wiki_dbs`, using only data from `period` and write to `path`.
    If `filter_disputed_reverts` is True, all reverts whose reverting edits were
    also reverted will be removed. Similarly, if `filter_anonymous_edits` is True,
    all edits by ip users will be removed.
    """
    revisions_df = get_revision_history(
        spark=spark,
        snapshot=snapshot,
        wiki_dbs=wiki_dbs,
    )

    if drop_anonymous_edits:
        revisions_df = revisions_df.filter(
            ~F.col("event_user_is_anonymous"),
        )
    if drop_disputed_reverts:
        revisions_df = revisions_df.transform(
            filter_disputed_reverts(reverting_rev_df=revisions_df),
        )

    gte_period_start = F.col("rev_timestamp") >= f"{period.start}"
    lte_period_end = F.col("rev_timestamp") <= f"{period.end}"

    # Filter out all revisions newer than period end so that
    # subsequent joins are cheaper.
    revisions_df = revisions_df.filter(lte_period_end)

    # Filter to get revisions only from desired period but pass
    # the unfiltered df to the transformation as parent_df so that
    # we don't miss revisions with older parents.
    revisions_df = revisions_df.filter(gte_period_start).transform(
        with_parent_revision_history(parent_df=revisions_df)
    )

    shuffle_partitions = int(spark.conf.get("spark.sql.shuffle.partitions"), base=10)

    # Partitioning by just wiki_db when taking a stratified sample would lead
    # to large partitions that are also skewed. Instead we first give each row
    # a "partition" number based on the hash code of its page_id (MurmurHash).
    # page_id works well to distribute revisions across partitions because their
    # distribution across page_ids is fairly uniform with relatively few outliers.
    sampled_revs_df = (
        revisions_df.withColumn(
            "partition", F.abs(F.hash("page_id")) % shuffle_partitions
        )
        .transform(
            stratified_sample(
                cols=("partition", "wiki_db"),
                strata_size=max_rows_per_wiki // shuffle_partitions + 1,
                seed=42,
            )
        )
        .drop("partition")
    )

    wikitext_df = get_wikitext_history(
        spark=spark,
        snapshot=snapshot,
        wiki_dbs=wiki_dbs,
    )
    wikitext_df = wikitext_df.filter(lte_period_end)

    revision_data_df = (
        sampled_revs_df.transform(with_wikitext(wikitext_df, rev_id="parent_rev_id"))
        .withColumnRenamed("rev_text", "parent_rev_text")
        .transform(with_wikitext(wikitext_df.filter(gte_period_start)))
    )
    revision_data_df = revision_data_df.withColumn(
        colName="lang",
        col=F.rtrim(F.regexp_replace("wiki_db", "wiki", " ")),
    )

    # Get all field names from the RevisionSchema in KI so that
    # we can make a struct out of columns of the same name and pass
    # that as json to the udf to extract features from it
    revision_fields = list(RevisionSchema.model_fields.keys())
    revision_data_json_col = F.to_json(F.struct(*revision_fields).alias("rev_json"))
    dataset_df = revision_data_df.select(
        "rev_id",
        "wiki_db",
        "page_id",
        "is_reverted",
        extract_features(revision_data_json_col).alias("features"),
    )
    # Repartitioning instead of coalesce because a drastic coalesce
    # can result in upstream stages taking place on fewer nodes than the
    # default partitioning. See the docs for coalesce for more info.
    dataset_df = dataset_df.repartition(numPartitions=max_files_per_wiki)
    return dataset_df


@app.command()
def main(
    snapshot: str = typer.Option(
        help="The mediawiki snapshot to read from",
        metavar="YYYY-MM",
        show_default=False,
    ),
    wikis: str = typer.Option(
        help=(
            "Comma separated list of wikis to include in the dataset. "
            "This is required as the pipeline is not designed to run "
            "on all wikis in one go."
        ),
        metavar="abcwiki,xyzwiki",
        show_default=False,
    ),
    max_rows_per_wiki: int = typer.Option(
        help="Max samples per wiki",
        default=300_000,
    ),
    max_files_per_wiki: int = typer.Option(
        help="Max files to write for each wiki partition",
        default=4,
    ),
    output: Path = typer.Option(
        help="Hdfs directory to write output to",
        show_default=False,
    ),
    start_time: datetime = typer.Option(
        help="Start of the sampling period",
        show_default=False,
    ),
    end_time: datetime = typer.Option(
        help="End of the sampling period",
        show_default=False,
    ),
    disputed_reverts: bool = typer.Option(
        help="Keep edit war reverts",
        default=False,
    ),
    anonymous_edits: bool = typer.Option(
        help="Keep all ip edits",
        default=False,
    ),
) -> None:
    """Generate a dataset for the revert risk multilingual model."""
    spark = SparkSession.builder.getOrCreate()
    dataset_df = generate_dataset(
        spark=spark,
        snapshot=snapshot,
        wiki_dbs=wikis.split(","),
        period=Period(start=start_time, end=end_time),
        drop_disputed_reverts=not disputed_reverts,
        drop_anonymous_edits=not anonymous_edits,
        max_rows_per_wiki=max_rows_per_wiki,
        max_files_per_wiki=max_files_per_wiki,
    )
    dataset_df.write.partitionBy("wiki_db").mode("overwrite").format("parquet").save(
        str(output)
    )
    spark.stop()


def entry_point() -> None:
    """Run the typer cli application. This function serves as an
    entry point to this application, allowing typer to parse arguments
    before passing them to `main`. Use this instead of `main` if you're
    defining a console script for this module.
    """
    app()


if __name__ == "__main__":
    app()
