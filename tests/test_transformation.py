import chispa
from pyspark.sql import SparkSession

from revert_risk_datasets.transformation import (
    filter_disputed_reverts,
    stratified_sample,
    with_parent_revision_history,
    with_wikitext,
)


def test_statified_sample(spark: SparkSession) -> None:
    cols = ("wiki_db", "rev_id")
    data = (
        ("abwiki", 1234),
        ("cdwiki", 4567),
        ("dewiki", 7891),
        ("abwiki", 1234),
    )
    df = spark.createDataFrame(data, cols)

    actual_df = df.transform(
        stratified_sample(cols=("wiki_db",), strata_size=1, seed=42)
    )

    expected_data = (
        ("abwiki", 1234),
        ("cdwiki", 4567),
        ("dewiki", 7891),
    )
    expected_df = spark.createDataFrame(expected_data, cols)

    chispa.assert_df_equality(actual_df, expected_df, ignore_row_order=True)


def test_filter_disputed_reverts(spark: SparkSession) -> None:
    cols = ("wiki_db", "rev_id", "page_id", "is_reverted", "reverting_rev_id")
    data = (
        ("abwiki", 1234, 1, True, 1011),
        ("dewiki", 7891, 2, True, 1011),
        ("abwiki", 1011, 1, True, 1112),
        ("cdwiki", 4567, 1, False, None),
    )
    df = spark.createDataFrame(data, cols)

    actual_df = df.transform(filter_disputed_reverts(reverting_rev_df=df))

    expected_data = (
        ("dewiki", 7891, 2, True, 1011),
        ("abwiki", 1011, 1, True, 1112),
        ("cdwiki", 4567, 1, False, None),
    )
    expected_df = spark.createDataFrame(expected_data, cols)

    chispa.assert_df_equality(actual_df, expected_df, ignore_row_order=True)


def test_with_parent_revision_history(spark: SparkSession) -> None:
    cols = ("wiki_db", "rev_id", "parent_rev_id", "is_reverted")
    data = (
        ("abwiki", 1234, 1011, True),
        ("dewiki", 7891, 1011, True),
        ("abwiki", 1011, 1112, True),
        ("cdwiki", 4567, None, False),
    )
    df = spark.createDataFrame(data, cols)

    actual_df = df.transform(with_parent_revision_history(parent_df=df))

    expected_data = (("abwiki", 1234, 1011, True, 1112, True),)
    parent_cols = ("parent_parent_rev_id", "parent_is_reverted")
    expected_df = spark.createDataFrame(expected_data, (*cols, *parent_cols))

    chispa.assert_df_equality(actual_df, expected_df, ignore_row_order=True)


def test_with_wikitext(spark: SparkSession) -> None:
    cols = ("wiki_db", "rev_id")
    data = (
        ("abwiki", 1234),
        ("dewiki", 7891),
        ("cdwiki", 4567),
    )
    df = spark.createDataFrame(data, cols)
    wikitext_data = (
        ("abwiki", 1234, "ab wikitext"),
        ("dewiki", 1112, "de wikitext"),
        ("ghwiki", 4567, "gh wikitext"),
    )
    wikitext_df = spark.createDataFrame(
        wikitext_data, ("wiki_db", "rev_id", "rev_text")
    )

    actual_df = df.transform(with_wikitext(wikitext_df=wikitext_df))

    expected_data = (("abwiki", 1234, "ab wikitext"),)
    expected_df = spark.createDataFrame(expected_data, (*cols, "rev_text"))

    chispa.assert_df_equality(actual_df, expected_df, ignore_row_order=True)
